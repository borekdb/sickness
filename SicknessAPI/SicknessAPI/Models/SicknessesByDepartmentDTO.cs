﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class SicknessesByDepartmentsDTO
    {
        public string Department { get; set; }
        public int SickEmployeesNumber { get; set; }
    }
}