﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class SicknessContext :DbContext
    {
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Sickness> Sickness { get; set; }        
        public SicknessContext() : base("SicknessAPI")
        {
        }
    }
}