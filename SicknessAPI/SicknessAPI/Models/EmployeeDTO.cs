﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class EmployeeDTO
    {
        public int? Id { get; set; }

        [Required]
        [MaxLength(5)]
        public string ExternalId { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(25)]
        //[RegularExpression(@"^[\p{Lu}\p{M}][\p{L}\p{M},]+(?: [\p{L}\p{M},]+)*$",
        //    ErrorMessage ="Błądne imię.")]
        public string Firstname { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(25)]
        //[RegularExpression(@"^[\p{Lu}\p{M}][\p{L}\p{M}-]+(?: [\p{L}\p{M}-]+)*$",
        //    ErrorMessage = "Błędne nazwisko.")]
        public string Lastname { get; set; }

        public SexType SexType { get; set; }
       
        public DateTime DateOfBirth { get; set; }
        
        public DateTime EmploymentDate { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(25)]
        //[RegularExpression(@"^[\p{Lu}\p{M}][\p{L}\p{M}.-]+(?: [\p{L}\p{M}.-]+)*$",
        //    ErrorMessage = "Błędne stanowisko.")]
        public string Position { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(25)]
        //[RegularExpression(@"^[\p{Lu}\p{M}][\p{L}\p{M}.-]+(?: [\p{L}\p{M}.-]+)*$",
        //    ErrorMessage = "Użyto niewłaściwych znaków")]
        public string Department { get; set; }

        public WorkerType WorkerType { get; set; }
        
        public DateTime? DismissalDate { get; set; }
    }
}