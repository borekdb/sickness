﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SicknessAPI.Models
{
    public class SicknessDTO
    {
        public int? Id { get; set; }
        public int EmployeeId { get; set; }
       
        //[Column(TypeName = "Date")]
        public DateTime DateFrom { get; set; }
        
        //[Column(TypeName = "Date")]
        public DateTime DateTo { get; set; }        
    }
}