﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class Sickness
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }

        [Column(TypeName = "Date")]
        public DateTime DateFrom { get; set; }

        [Column(TypeName = "Date")]
        public DateTime DateTo { get; set; }

        public Employee Employee { get; set; }
    }
}