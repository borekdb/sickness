﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class SicknessesByAgeDTO
    {
        public byte AgeRangeFrom { get; set; }
        public byte AgeRangeTo { get; set; }
        public int SickEmployeesNumber { get; set; }
        public int TotalSickDays { get; set; }
        public int AgeEmployee { get; set; }
        public int SickDaysNumber { get; set; }
        public int SickId { get; set; } //TODO: pole wprowadzone tylko do celów testowych można usunąć
    }
}