﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class SicknessesByWorkerTypeDTO
    {
        public WorkerType WorkerType { get; set; }
        public int SickEmployeesNumber { get; set; }
    }
}