﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class Employee
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(5)]
        public string ExternalId { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(25)]
        public string Firstname { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(25)]
        public string Lastname { get; set; }

        public SexType SexType { get; set; }

        [Column(TypeName = "Date")]
        public DateTime DateOfBirth { get; set; }

        [Column(TypeName = "Date")]
        public DateTime EmploymentDate { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(25)]
        public string Position { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(25)]
        public string Department { get; set; }

        public WorkerType WorkerType { get; set; }

        [Column(TypeName = "Date")]
        public DateTime? DismissalDate { get; set; }
    }
}