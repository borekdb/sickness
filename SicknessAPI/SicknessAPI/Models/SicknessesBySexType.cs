﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class SicknessesBySexTypeDTO
    {
        public SexType SexType { get; set; }
        public int SickEmployeesNumber { get; set; }
    }
}