﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SicknessAPI.Models
{
    public class RangeDatesDTO
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
    }
}