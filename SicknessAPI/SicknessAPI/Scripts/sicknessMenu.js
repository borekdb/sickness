﻿var employeesList = new Array();
var spinner = new bootstrap.Modal(document.getElementById('spinner'));

function loadData() {

    $("#mainRow").hide();
    spinner.show();
    getEmployees().then(function () {
        getSicknesses();
    });
}

function getEmployees() {
    return axios.get('https://localhost:44307/api/Sickness/GetAllEmployees')
        .then(function (response) {

            var { data: employees } = response;

            if (employees.length > 0) {
                employeesList = employees
                return;
            }
            showMessage('error', 'Błąd', 'Lista pracowników jest pusta');
        })
        .catch(function (error) {
            showMessage("error", "Błąd", "Problem z pobraniem listy pracowników.");
        })
        .then(function () {
        });
}

function getSicknesses() {

    axios.get('https://localhost:44307/api/Sickness/GetAllSicknesses')
        .then(function (response) {

            var { data: sicknesses } = response;

            if (sicknesses.length > 0) {

                $("#mainRow").toggle("slow");

                document.querySelector('#sicknessTable tbody').innerHTML = "";

                const createTbody = document.querySelector('#sicknessTable tbody');

                for (var i = 0; i < sicknesses.length; i++) {

                    var sickEmployee = employeesList.find(x => x.Id == sicknesses[i].EmployeeId);

                    var row = createTbody.insertRow(i)
                    var cell = row.insertCell(0);
                    cell.innerHTML = sickEmployee.ExternalId;
                    cell = row.insertCell(1);
                    cell.innerHTML = sickEmployee.Lastname.toUpperCase() + " " + sickEmployee.Firstname;
                    cell = row.insertCell(2);
                    cell.innerHTML = moment(sicknesses[i].DateFrom).format('DD-MM-YYYY');
                    cell = row.insertCell(3);
                    cell.innerHTML = moment(sicknesses[i].DateTo).format('DD-MM-YYYY');
                    cell = row.insertCell(4);
                    var sicknessId = sicknesses[i].Id;
                    cell.innerHTML =
                        '<button class="btn btn-outline-primary" onclick="showSicknessModal(' + sicknessId + ')">Edycja</button>'
                        + '<button class="btn btn-outline-danger" onclick="sicknessDelete(' + sicknessId + ')">Usuń</button>';
                    //employeesList.push(sicknesses[i].Employee);
                }
            }
            else {
                showMessage('error', 'Błąd', 'Lista zwolnień lekarskich jest pusta.');
            }
        })
        .catch(function (error) {
            $("#mainContainer").hide("fast");
            showMessage("error", "Błąd", "Problem z pobraniem danych o zwolnieniach lekarskich.");
        })
        .then(function () {
            setTimeout(function () {
                spinner.hide();
            }, 500);
        });
}

function saveSickness() {

    if ($("#sicknessForm").valid()) {

        if (document.getElementById("sicknessTo").value < document.getElementById("sicknessFrom").value) {
            showMessage('error', 'Błąd dat.', 'Data początku zwolnienia jest poźniejsza niż data końca.');
            return;
        }
        else {
            spinner.show();
            axios.post('https://localhost:44307/api/Sickness/SaveSickness',
                {
                    Id: document.getElementById("sicknessId").value,
                    EmployeeId: document.getElementById("id").value,
                    DateFrom: document.getElementById("sicknessFrom").value,
                    DateTo: document.getElementById("sicknessTo").value,
                    //document.getElementById("sicknessId").value, 
                })
                .then(function (response) {
                    setTimeout(function () {
                        spinner.hide();
                    }, 500);
                    $("#sicknessModal").modal("hide");
                    loadData();
                    Swal.fire({
                        position: 'top',
                        icon: 'success',
                        title: 'Zapis zakończony powodzeniem.',
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
                .catch(function (error) {
                    showMessage('error', 'Błąd.', 'Zwolnienie nie zostało dodane.');
                });
        }
    }
}

function showSicknessModal(sicknessId) {

    var sicknessModal = new bootstrap.Modal(document.getElementById('sicknessModal'));
    document.querySelectorAll("#sicknessForm input, #sicknessForm select").forEach(function (element) {
        element.value = "";
    });
    sicknessModal.show();

    if (employeesList.length > 0) {

        for (var i = 0; i < employeesList.length; i++) {
            $('#employeeSelector').append(new Option
                (
                    employeesList[i].Lastname.toUpperCase()
                    + "   "
                    + employeesList[i].Firstname
                    + " \xa0\xa0\xa0\xa0\xa0 Data urodzenia: " + moment(employeesList[i].DateOfBirth).format('YYYY-MM-DD')
                    , employeesList[i].Id
                )
            );
        }
        if (sicknessId > 0) {
            document.querySelector("#sicknessModal .modal-title").innerHTML = "Edycja zwolnienia lekarskiego.";

            axios.get('https://localhost:44307/api/Sickness/GetOneSickness?sicknessId=' + sicknessId)
                .then(function (response) {
                    var { data: sickness } = response;

                    var sickEmployee = employeesList.find(x => x.Id == sickness.EmployeeId);

                    document.querySelector("#sicknessModal .modal-title").innerHTML = "Edycja zwolnienia lekarskiego";
                    document.getElementById("sicknessId").value = sickness.Id;
                    document.getElementById("id").value = sickEmployee.Id;
                    document.getElementById("egeriaId").value = sickEmployee.ExternalId;
                    document.getElementById("sicknessFrom").value = moment(sickness.DateFrom).format('YYYY-MM-DD');
                    document.getElementById("sicknessTo").value = moment(sickness.DateTo).format('YYYY-MM-DD');
                    document.getElementById("employeeSelector").value = sickEmployee.Id;
                })
                .catch(function (error) {
                    $("#mainRow").hide();
                    showMessage("error", "Błąd", "Problem z pobraniem danych.");
                })
                .then(function () {

                });
        }
        else {
            document.querySelector("#sicknessModal .modal-title").innerHTML = "Nowe zwolnienie lekarskie";
        }
        setTimeout(function () {
            spinner.hide();
        }, 500);
    }
    else {
        showMessage('error', 'Błąd', 'Nie pobrano listy pracowników.');
    }
}

function updateEmployeeData() {
    var selectedEmployeeId = parseInt(document.getElementById("employeeSelector").value);
    var selectedEmployee = employeesList.find(x => x.Id == selectedEmployeeId);

    document.getElementById("id").value = selectedEmployee.Id;
    document.getElementById("egeriaId").value = selectedEmployee.ExternalId;
}

function employeeDelete(employeeId) {
    console.log(employeeId);
}

function showMessage(icon, title, text) {
    Swal.fire({
        icon: icon,
        title: title,
        text: text
    })
}

window.onscroll = function () {
    scrollFunction();
};

function scrollFunction() {
    if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
    ) {
        $('#scrollUpDiv').removeClass('d-none').addClass('d-none d-lg-block');

        //scrollUpBtn.style.display = "block";
    } else {
        $('#scrollUpDiv').removeClass('d-lg-block').addClass('d-none');
    }
}

function scrollToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}