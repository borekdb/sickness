﻿function showSide() {
    $('#mainContainer').toggle('slow').removeClass('d-none');
    document.getElementById('chartHeader').value = "dkajhuidsfdiusfhiushfiu";
}
function createAgeChart() {

    var isDatesCorrect = datesValidate("Sprawdź poprawność wprowadzonych dat.");

    if (isDatesCorrect) {

        var DateStart = document.getElementById("dateFrom").value;
        var DateEnd = document.getElementById("dateTo").value;
        $('#myChart').remove();
        var spinner = new bootstrap.Modal(document.getElementById("spinner"));
        spinner.show();

        if (DateEnd >= DateStart) {

            axios.get('https://localhost:44307/api/Sickness/CategorizationSicknessesByAge', {
                params: {
                    DateStart,
                    DateEnd
                }
            })
                .then(function (response) {

                    var { data: ageDivision } = response;



                    if (ageDivision.length > 0) {

                        var newCanvas = document.createElement('canvas');
                        newCanvas.id = "myChart";
                        document.getElementById('chart').appendChild(newCanvas);

                        var quantitySickDays = [];
                        for (var i = 0; i < ageDivision.length; i++) {

                            quantitySickDays[i] = ageDivision[i].TotalSickDays;
                        };
                        //document.querySelector('.col-md-8, h4').innerHTML = 'Łączna ilość dni chorobowego w grupach wiekowych:';
                        document.getElementById('chartHeader').innerHTML = "Łączna ilość dni chorobowego w grupach wiekowych:";

                        // setup block
                        const data = {
                            labels: ['poniżej 25: ' + quantitySickDays[0] + ' dni',
                            'od 26 do 35 lat: ' + quantitySickDays[1] + ' dni',
                            'od 36 do 45 lat: ' + quantitySickDays[2] + ' dni',
                            'od 46 do 55 lat: ' + quantitySickDays[3] + ' dni',
                            'od 56 do 65 lat: ' + quantitySickDays[4] + ' dni',
                            'powyżej 66 lat: ' + quantitySickDays[5] + ' dni'
                            ],
                            datasets: [{
                                label: 'Łączna ilość dni chorobowego w grupach wiekowych:',
                                data: quantitySickDays,
                                backgroundColor: [
                                    'rgba(255, 99, 132, 0.2)',
                                    'rgba(54, 162, 235, 0.2)',
                                    'rgba(255, 206, 86, 0.2)',
                                    'rgba(75, 192, 192, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                hoverOffset: 4,
                                borderColor: [
                                    'rgba(255, 99, 132, 1)',
                                    'rgba(54, 162, 235, 1)',
                                    'rgba(255, 206, 86, 1)',
                                    'rgba(75, 192, 192, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 1
                            }]
                        };
                        // option block
                        const options = {
                            plugins:
                            {
                                tooltip: {
                                    displayColors: false,
                                    bodySpacing: 15,
                                    bodyAlign: 'center',
                                    backgroundColor: '#0e71bb',

                                    bodyFont: {
                                        size: 20
                                    },
                                    callbacks: {
                                        label: function (tooltipItem) {
                                            let label = pieChart.data.labels[tooltipItem.dataIndex];
                                            return label;
                                        }
                                    }
                                },

                                //title: {
                                //    display: true,
                                //    text: 'Łączna ilość dni chorobowego w grupach wiekowych:',
                                //    font: {
                                //        size: 20
                                //    },
                                //    color: '#B22222'
                                //},

                                legend: {
                                    position: 'right',
                                    align: 'start',
                                    labels: {
                                        font: {
                                            size: 15
                                        }
                                    }
                                }
                            }
                        };
                        // config block
                        const config = {
                            type: 'pie',
                            data,
                            options
                        };

                        // render block 
                        const pieChart = new Chart(
                            document.getElementById('myChart'),
                            config
                        )
                    }
                    else {
                        showMessage('error', 'Błąd', 'Brak danych spełniających kryteria.');
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    showMessage('error', 'Błąd', 'Problem z pobraniem danych.');
                })
                .then(function () {
                    setTimeout(function () {
                        spinner.hide();
                    }, 500);

                });
        }
    }
}
function createSexTypeChart() {

    var isDatesCorrect = datesValidate("Sprawdź poprawność wprowadzonych dat.");

    if (isDatesCorrect) {

        var DateStart = document.getElementById("dateFrom").value;
        var DateEnd = document.getElementById("dateTo").value;
        $('#myChart').remove();
        var spinner = new bootstrap.Modal(document.getElementById("spinner"));
        spinner.show();

        if (DateEnd >= DateStart) {

            axios.get('https://localhost:44307/api/Sickness/CategorizationSicknessesBySexType', {
                params: {
                    DateStart,
                    DateEnd
                }
            })
                .then(function (response) {

                    var { data: sexTypeDivision } = response;

                    if (sexTypeDivision.length > 0) {

                        var newCanvas = document.createElement('canvas');
                        newCanvas.id = "myChart";
                        document.getElementById('chart').appendChild(newCanvas);

                        var quantitySickDays = [];
                        for (var i = 0; i < sexTypeDivision.length; i++) {

                            quantitySickDays[i] = sexTypeDivision[i].SickEmployeesNumber;
                        }
                        //document.querySelector('#chart, h4').innerHTML = 'Zwolnienia pod względem płci.';

                        // setup block
                        const data = {
                            labels: ['Kobiety: ' + quantitySickDays[0],
                            'Mężczyźni: ' + quantitySickDays[1]],
                            datasets: [{
                                label: 'Ilość zwolnień lekarskich z podziałem na płeć.',
                                data: quantitySickDays,
                                backgroundColor: [
                                    'rgba(255, 100, 193, 0.2)',
                                    'rgba(54, 162, 235, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255, 100, 193, 1)',
                                    'rgba(54, 162, 235, 1)'
                                ],
                                borderWidth: 1
                            }]
                        }
                        // option block
                        const options = {
                            plugins:
                            {
                                tooltip: {
                                    displayColors: false,
                                    bodySpacing: 15,
                                    bodyAlign: 'center',
                                    backgroundColor: '#0e71bb',
                                    titleMarginBottom: 15,

                                    bodyFont: {
                                        size: 20
                                    },
                                    callbacks: {
                                        label: function (tooltipItem) {
                                            let label = pieChart.data.labels[tooltipItem.dataIndex];
                                            return label;
                                        }
                                    }
                                },

                                title: {
                                    display: true,
                                    text: 'Ilość zwolnień lekarskich z podziałem na płeć:',
                                    font: {
                                        size: 20
                                    },
                                    color: '#B22222'
                                },

                                legend: {
                                    position: 'top',
                                    align: 'center',
                                    labels: {
                                        font: {
                                            size: 19
                                        }
                                    }
                                }
                            }
                        }
                        // config block
                        const config = {
                            type: 'pie',
                            data,
                            options
                        }
                        // render block 
                        const pieChart = new Chart(
                            document.getElementById('myChart'),
                            config
                        )
                    }
                    else {
                        showMessage('error', 'Błąd', 'Brak danych spełniających kryteria.');
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    showMessage('error', 'Błąd', 'Problem z pobraniem danych.');
                })
                .then(function () {
                    setTimeout(function () {
                        spinner.hide();
                    }, 500);
                });
        }
    }
}

function createDepartmentsChart() {

    var isDatesCorrect = datesValidate("Sprawdź poprawność wprowadzonych dat.");

    if (isDatesCorrect) {
        var DateStart = document.getElementById("dateFrom").value;
        var DateEnd = document.getElementById("dateTo").value;
        $('#myChart').remove();
        var spinner = new bootstrap.Modal(document.getElementById("spinner"));
        spinner.show();
        setTimeout(function () {
            spinner.hide();
        },500);
        showMessage('info', "Informacja", "Prcujemy nad tym. :)");
    }
}

function createWorkerTypeChart() {

    var isDatesCorrect = datesValidate("Sprawdź poprawność wprowadzonych dat.");

    if (isDatesCorrect) {
        var DateStart = document.getElementById("dateFrom").value;
        var DateEnd = document.getElementById("dateTo").value;
        $('#myChart').remove();
        var spinner = new bootstrap.Modal(document.getElementById("spinner"));
        spinner.show();

        if (DateEnd >= DateStart) {

            axios.get('https://localhost:44307/api/Sickness/CategorizationByWorkerType', {
                params: {
                    DateStart,
                    DateEnd
                }
            })
                .then(function (response) {

                    var { data: workerTypeDivision } = response;

                    if (workerTypeDivision.length > 0) {

                        var newCanvas = document.createElement('canvas');
                        newCanvas.id = "myChart";
                        document.getElementById('chart').appendChild(newCanvas);

                        var quantitySickDays = [];
                        for (var i = 0; i < workerTypeDivision.length; i++) {

                            quantitySickDays[i] = workerTypeDivision[i].SickEmployeesNumber;
                        }
                        spinner.hide();
                        //document.querySelector('#chart, h4').innerHTML = 'Zwolnienia pod względem płci.';

                        // setup block
                        const data = {
                            labels: ['Umysłowa: ' + quantitySickDays[0],
                            'Fizyczna: ' + quantitySickDays[1]],
                            datasets: [{
                                label: 'Ilość zwolnień lekarskich z podziałem na rodzaj pracy:',
                                data: quantitySickDays,
                                backgroundColor: [
                                    'rgba(215, 72, 38, 0.2)',
                                    'rgba(191, 185, 177, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(215, 72, 38, 1)',
                                    'rgba(75, 54, 33, 1)'
                                ],
                                borderWidth: 1
                            }]
                        }
                        // option block
                        const options = {
                            plugins:
                            {
                                tooltip: {
                                    displayColors: false,
                                    bodySpacing: 15,
                                    bodyAlign: 'center',
                                    backgroundColor: 'rgba(215, 72, 38, 0.0)',
                                    titleMarginBottom: 15,

                                    bodyFont: {
                                        size: 20
                                    },
                                    bodyColor: Array[
                                        'rgba(215, 72, 38, 1)',
                                        'rgba(75, 54, 33, 1)'],
                                    bodyColor: function (tooltipItem) {
                                        console.log(tooltipItem);
                                        console.log(pieChart.data.borderColor[0]);
                                        const fontColor = tooltipItem.tooltip.labelColors[0].borderColor;
                                        console.log(fontColor);
                                        return fontColor;
                                    },
                                    callbacks: {
                                        label: function (tooltipItem) {
                                            let label = pieChart.data.labels[tooltipItem.dataIndex];
                                            return label;
                                        }
                                    }
                                },

                                title: {
                                    display: true,
                                    text: 'Ilość zwolnień lekarskich z podziałem na rodzaj pracy:',
                                    font: {
                                        size: 20,
                                    },
                                    color: '#B22222'

                                },

                                legend: {
                                    position: 'top',
                                    align: 'center',
                                    fullWidth: false,
                                    labels: {
                                        font: {
                                            size: 19,
                                        },
                                        color: [
                                            'rgba(215, 72, 38, 1)',
                                            'rgba(75, 54, 33, 1)'
                                        ]
                                    }
                                }
                            }
                        }
                        // config block
                        const config = {
                            type: 'pie',
                            data,
                            options
                        }
                        // render block 
                        const pieChart = new Chart(
                            document.getElementById('myChart'),
                            config
                        )


                    }
                    else {
                        showMessage('error', 'Błąd', 'Brak danych spełniających kryteria.');
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    showMessage('error', 'Błąd', 'Problem z pobraniem danych.');
                })
                .then(function () {
                    setTimeout(function () {
                        spinner.hide();
                    }, 500);
                });
        }
    }
}
function datesValidate(message) {
    if ($('#datesForm').valid()) {
        return true;
    }
    else {
        showMessage('error', 'Błąd', message);
        return false;
    }
}

function showMessage(icon, title, text) {
    Swal.fire({
        icon: icon,
        title: title,
        text: text
    })
}