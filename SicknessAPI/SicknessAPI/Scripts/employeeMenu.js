﻿function loadData() {

    $("#mainRow").hide();

    var spinner = new bootstrap.Modal(document.getElementById("spinner"));
    spinner.show();

    axios.get('https://localhost:44307/api/Sickness/GetAllEmployees')
        .then(function (response) {

            var { data: employeesTable } = response;

            if (employeesTable.length > 0) {

                $("#mainRow").toggle("slow");

                document.querySelector('#employeeTable tbody').innerHTML = "";

                const createTbody = document.querySelector('#employeeTable tbody');
                for (var i = 0; i < employeesTable.length; i++) {
                    var row = createTbody.insertRow(i)
                    var cell = row.insertCell(0);
                    cell.innerHTML = employeesTable[i].ExternalId;
                    cell = row.insertCell(1);
                    cell.innerHTML = employeesTable[i].Firstname;
                    cell = row.insertCell(2);
                    cell.innerHTML = employeesTable[i].Lastname;
                    cell = row.insertCell(3);
                    var employeeId = employeesTable[i].Id;
                    cell.innerHTML =
                        '<button class="btn btn-outline-primary" onclick="showEmployeeModal(' + employeeId + ')">Edycja</button>'
                        + '<button class="btn btn-outline-danger" onclick="employeeDelete(' + employeeId + ')">Usuń</button>';
                }
            }
            else {
                showMessage('error', 'Błąd', 'Lista pracowników jest puta');
            }
        })
        .catch(function (error) {
            showMessage('error', 'Błąd', 'Problem z pobraniem danych.');
        })
        .then(function () {
            setTimeout(function () {
                spinner.hide();
            }, 500);
        });
}

function showEmployeeModal(employeeId) {

    var spinner = new bootstrap.Modal(document.getElementById("spinner"));
    spinner.show();
    if (employeeId > 0) {

        axios.get("https://localhost:44307/api/Sickness/GetOneEmployee?id=" + employeeId)
            .then(function (response) {
                var { data: employee } = response;

                var employeeModal = new bootstrap.Modal(document.getElementById('employeeModal'));
                employeeModal.show();

                document.querySelector("#employeeModal .modal-title").innerHTML = "Edycja danych pracownika";

                document.getElementById("id").value = employee.Id;
                document.getElementById("egeriaId").value = employee.ExternalId;
                document.getElementById("firstname").value = employee.Firstname;
                document.getElementById("lastname").value = employee.Lastname;
                document.getElementById("sexType").value = employee.SexType;
                document.getElementById("dateOfBirth").value = moment(employee.DateOfBirth).format('YYYY-MM-DD');
                document.getElementById("position").value = employee.Position;
                document.getElementById("employmentDate").value = moment(employee.EmploymentDate).format('YYYY-MM-DD');
                document.getElementById("department").value = employee.Department;
                document.getElementById("workerType").value = employee.WorkerType;
                document.getElementById("dismissalDate").value = moment(employee.DismissalDate).format('YYYY-MM-DD');
            })
            .catch(function (error) {
                showMessage('error', 'Błąd', 'Problem z pobraniem danych.');
            })
            .then(function () {
                setTimeout(function () {
                    spinner.hide();
                }, 500);
            });
    }
    else {
        var employeeModal = new bootstrap.Modal(document.getElementById('employeeModal'));
        document.querySelectorAll("#employeeForm input, #sicknessForm select").forEach(function (element) {
            element.value = "";
        });
        employeeModal.show();
        setTimeout(function () {
            spinner.hide();
        }, 500);
        document.querySelector("#employeeModal .modal-title").innerHTML = "Dane nowego pracownika";
    }
}
function saveEmployee() {

    if ($("#employeeForm").valid()) {

        var spinner = new bootstrap.Modal(document.getElementById('spinner'));
        spinner.show();

        axios.post('https://localhost:44307/api/Sickness/SaveEmployee',
            {
                Id: document.getElementById("id").value,
                ExternalId: document.getElementById("egeriaId").value,
                Firstname: document.getElementById("firstname").value,
                Lastname: document.getElementById("lastname").value,
                DateOfBirth: document.getElementById("dateOfBirth").value,
                Department: document.getElementById("department").value,
                EmploymentDate: document.getElementById("employmentDate").value,
                Position: document.getElementById("position").value,
                SexType: document.getElementById("sexType").value,
                WorkerType: document.getElementById("workerType").value,
                DismissalDate: document.getElementById("dismissalDate").value
            })
            .then(function (response) {
                $("#employeeModal").modal("hide");
                setTimeout(function () {
                    spinner.hide();
                }, 100);
                Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Zapis zakończony powodzeniem.',
                    showConfirmButton: false,
                    timer: 1500
                })
                loadData();
            })
            .catch(function (error) {
                showMessage('error', 'Błąd', 'Pracownik nie został dodany.');
            });
    }
}

function employeeDelete(employeeId) {
    console.log(employeeId);
}

window.onscroll = function () {
    scrollFunction();
};

function scrollFunction() {
    if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
    ) {
        $('#scrollUpDiv').removeClass('d-none').addClass('d-none d-lg-block');
    } else {
        $('#scrollUpDiv').removeClass('d-lg-block').addClass('d-none');
    }
}

function scrollToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}