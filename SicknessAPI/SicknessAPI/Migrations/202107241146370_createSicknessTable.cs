﻿namespace SicknessAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createSicknessTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sicknesses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        DateFrom = c.DateTime(nullable: false, storeType: "date"),
                        DateTo = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sicknesses", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Sicknesses", new[] { "EmployeeId" });
            DropTable("dbo.Sicknesses");
        }
    }
}
