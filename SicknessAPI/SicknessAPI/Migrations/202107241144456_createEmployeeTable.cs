﻿namespace SicknessAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createEmployeeTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExternalId = c.String(nullable: false, maxLength: 5),
                        Firstname = c.String(nullable: false, maxLength: 25),
                        Lastname = c.String(nullable: false, maxLength: 25),
                        SexType = c.Int(nullable: false),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        EmploymentDate = c.DateTime(nullable: false, storeType: "date"),
                        Position = c.String(nullable: false, maxLength: 25),
                        Department = c.String(nullable: false, maxLength: 25),
                        WorkerType = c.Int(nullable: false),
                        DismissalDate = c.DateTime(storeType: "date"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Employees");
        }
    }
}
