﻿using SicknessAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Data.Entity;
using System.Runtime.Serialization;
using System.Web.Http.Cors; // można odłączyć tego usinga


namespace SicknessAPI.Controllers
{
    //[EnableCors("*", "*", "*")]
    public class SicknessController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> SaveEmployee([FromBody] EmployeeDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (model.DateOfBirth >= model.EmploymentDate || model.DateOfBirth.AddYears(15) > model.EmploymentDate)
                {
                    return BadRequest("Błąd dat urodzenie/zatrudnienie, lub pracownik jest zbyt młody.");
                }
                if (model.DateOfBirth > model.DismissalDate || model.EmploymentDate > model.DismissalDate)
                {
                    return BadRequest("Błąd w dacie zwolnienia");
                }
                if (model.Id > 0)
                {
                    using (var context = new SicknessContext())
                    {
                        var updateEmployee = await context.Employee.Where(x => x.Id == model.Id).SingleOrDefaultAsync();

                        //updateEmployee.Firstname = char.ToUpper(receivedEmployee.Firstname[0])
                        //  + receivedEmployee.Firstname.Substring(1).ToLower();

                        //updateEmployee.Lastname = char.ToUpper(receivedEmployee.Lastname[0])
                        //    + receivedEmployee.Lastname.Substring(1).ToLower();
                        if (updateEmployee == null)
                        {
                            return BadRequest("Nie znaleziono danych.");
                        }
                        updateEmployee.Firstname = model.Firstname;
                        updateEmployee.Lastname = model.Lastname;
                        updateEmployee.DateOfBirth = model.DateOfBirth;
                        updateEmployee.Department = model.Department;
                        updateEmployee.EmploymentDate = model.EmploymentDate;
                        updateEmployee.ExternalId = model.ExternalId;
                        updateEmployee.Position = model.Position;
                        updateEmployee.SexType = model.SexType;
                        updateEmployee.WorkerType = model.WorkerType;
                        updateEmployee.DismissalDate = model.DismissalDate;

                        await context.SaveChangesAsync();
                    }
                    return Ok();
                }
                else
                {
                    using (var context = new SicknessContext())
                    {
                        context.Employee.Add(new Employee()
                        {
                            ExternalId = model.ExternalId,
                            Firstname = model.Firstname,
                            Lastname = model.Lastname,
                            Position = model.Position,
                            Department = model.Department,
                            SexType = model.SexType,
                            DateOfBirth = model.DateOfBirth,
                            EmploymentDate = model.EmploymentDate,
                            WorkerType = model.WorkerType,
                            DismissalDate = model.DismissalDate,
                            //Firstname = char.ToUpper(receivedEmployee.Firstname[0])
                            //+ receivedEmployee.Firstname.Substring(1).ToLower(),
                        });
                        await context.SaveChangesAsync();
                    }
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpPost]
        public async Task<IHttpActionResult> SaveSickness([FromBody] SicknessDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Błąd danych.");
                }
                if (model.DateFrom > model.DateTo)
                {
                    return BadRequest("Data od jest poźniejsza niż data do.");
                }
                Sickness existingSickness = null;
                using (var context = new SicknessContext())
                {
                    existingSickness = await context.Sickness
                        .Where(sick => sick.Id == model.Id)
                        .Include(emp => emp.Employee)
                        .SingleOrDefaultAsync();
                }
                if (existingSickness != null)
                {
                    if (existingSickness.Employee.DismissalDate != null)
                    {
                        if (model.DateFrom > existingSickness.Employee.DismissalDate || model.DateTo > existingSickness.Employee.DismissalDate)
                        {
                            return BadRequest("Nie można wprwadzić chorobowego. Pracownik w tym okresie był zwolniony.");
                        }
                        else
                        {
                            using (var context = new SicknessContext())
                            {
                                var updateSickness = await context.Sickness.Where(x => x.Id == model.Id).SingleAsync();
                                updateSickness.EmployeeId = model.EmployeeId;
                                updateSickness.DateFrom = model.DateFrom;
                                updateSickness.DateTo = model.DateTo;
                                await context.SaveChangesAsync();
                            }
                            return Ok();
                        }
                    }
                    else
                    {
                        using (var context = new SicknessContext())
                        {
                            var updateSickness = await context.Sickness.Where(x => x.Id == model.Id).SingleAsync();
                            updateSickness.EmployeeId = model.EmployeeId;
                            updateSickness.DateFrom = model.DateFrom;
                            updateSickness.DateTo = model.DateTo;
                            await context.SaveChangesAsync();
                        }
                        return Ok();
                    }                    
                }
                else
                {
                    using (var context = new SicknessContext())
                    {
                        context.Sickness.Add(new Sickness()
                        {
                            EmployeeId = model.EmployeeId,
                            DateFrom = model.DateFrom,
                            DateTo = model.DateTo
                        });
                        await context.SaveChangesAsync();
                    }
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetAllSicknesses()
        {
            try
            {
                List<Sickness> sicknessesDB = null;
                using (var context = new SicknessContext())
                {
                    sicknessesDB = await context.Sickness.ToListAsync();
                }
                if (sicknessesDB.Any())
                {
                    List<SicknessDTO> sicknessesDTO = new List<SicknessDTO>();
                    foreach (var item in sicknessesDB)
                    {
                        sicknessesDTO.Add(new SicknessDTO
                        {
                            Id = item.Id,
                            EmployeeId = item.EmployeeId,
                            DateFrom = item.DateFrom,
                            DateTo = item.DateTo
                        });
                    }
                    return Ok(sicknessesDTO);
                }
                else
                {
                    return BadRequest("Pobieranie danych nie powiodło się.");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetOneSickness([FromUri] int sicknessId)
        {
            try
            {
                Sickness sicknessDB = null;
                using (var context = new SicknessContext())
                {
                    sicknessDB = await context.Sickness
                        .Where(sick => sick.Id == sicknessId)
                        .Include(emp => emp.Employee)
                        .SingleAsync();
                }
                if (sicknessDB != null)
                {
                    SicknessDTO sicknessDTO = new SicknessDTO();
                    sicknessDTO.Id = sicknessDB.Id;
                    sicknessDTO.EmployeeId = sicknessDB.EmployeeId;
                    sicknessDTO.DateFrom = sicknessDB.DateFrom;
                    sicknessDTO.DateTo = sicknessDB.DateTo;
                    return Ok(sicknessDTO);
                }
                else
                {
                    return BadRequest("Nie pobrano danych");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetAllEmployees()
        {
            try
            {
                List<Employee> employeesDB = null;
                using (var context = new SicknessContext())
                {
                    employeesDB = await context.Employee.ToListAsync();
                }
                if (employeesDB.Any())
                {
                    employeesDB = employeesDB.OrderBy(x => x.Lastname).ThenBy(x => x.Firstname).ToList();
                    List<EmployeeDTO> employeesDTO = new List<EmployeeDTO>();
                    foreach (var item in employeesDB)
                    {
                        employeesDTO.Add(new EmployeeDTO
                        {
                            Id = item.Id,
                            ExternalId = item.ExternalId,
                            Firstname = item.Firstname,
                            Lastname = item.Lastname,
                            DateOfBirth = item.DateOfBirth,
                            Department = item.Department,
                            SexType = item.SexType,
                            DismissalDate = item.DismissalDate,
                            EmploymentDate = item.EmploymentDate,
                            Position = item.Position,
                            WorkerType = item.WorkerType

                        });
                    }
                    return Ok(employeesDTO);
                }
                return BadRequest("Nie pobrano danych");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetOneEmployee([FromUri] int id)
        {
            try
            {
                Employee employeeDB = null;
                using (var context = new SicknessContext())
                {
                    employeeDB = await context.Employee.SingleAsync(x => x.Id == id);
                }
                if (employeeDB != null)
                {
                    EmployeeDTO employeeDTO = new EmployeeDTO();
                    employeeDTO.Id = employeeDB.Id;
                    employeeDTO.ExternalId = employeeDB.ExternalId;
                    employeeDTO.EmploymentDate = employeeDB.EmploymentDate;
                    employeeDTO.Firstname = employeeDB.Firstname;
                    employeeDTO.Lastname = employeeDB.Lastname;
                    employeeDTO.SexType = employeeDB.SexType;
                    employeeDTO.Position = employeeDB.Position;
                    employeeDTO.WorkerType = employeeDB.WorkerType;
                    employeeDTO.DateOfBirth = employeeDB.DateOfBirth;
                    employeeDTO.Department = employeeDB.Department;
                    employeeDTO.DismissalDate = employeeDTO.DismissalDate;
                    return Ok(employeeDTO);
                }
                return BadRequest("Nie pobrano danych");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetEmployeesBasicData()
        {
            try
            {
                List<Employee> employeesList = null;
                using (var context = new SicknessContext())
                    //{
                    //    employeesList = await context.Employee.ToListAsync();
                    //}
                    employeesList = await context.Employee
                        .Select(e => new Employee()
                        {
                            Id = e.Id,
                            ExternalId = e.ExternalId,
                            Firstname = e.Firstname,
                            Lastname = e.Lastname,
                            DateOfBirth = e.DateOfBirth
                        }).ToListAsync();

                if (employeesList.Any())
                {
                    return Ok(employeesList);
                }
                return BadRequest("Nie pobrano żadnych danych");

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> CategorizationSicknessesByAge([FromUri] RangeDatesDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Błąd w dacie.");
                }
                if (model.DateStart > model.DateEnd)
                {
                    return BadRequest("Data od jest późniejszą niż data do.");
                }
                var sickEmployeesList = await CreateSickEmployeesInDatesRangeList(model.DateStart, model.DateEnd);
                if (sickEmployeesList.Count == 0)
                {
                    return Ok("Brak danych spełniających kryteria.");
                }
                foreach (var sickEmployee in sickEmployeesList)
                {
                    if (sickEmployee.DateFrom < model.DateStart)
                    {
                        sickEmployee.DateFrom = model.DateStart;
                    }
                    if (sickEmployee.DateTo > model.DateEnd)
                    {
                        sickEmployee.DateTo = model.DateEnd;
                    }
                    if (sickEmployee.DateFrom < model.DateStart && sickEmployee.DateTo > model.DateEnd)
                    {
                        sickEmployee.DateFrom = model.DateStart;
                        sickEmployee.DateTo = model.DateEnd;
                    }
                }
                byte ageRange1From = 16;
                byte ageRange1To = 25;
                byte ageRange2From = 26;
                byte ageRange2To = 35;
                byte ageRange3From = 36;
                byte ageRange3To = 45;
                byte ageRange4From = 46;
                byte ageRange4To = 55;
                byte ageRange5From = 56;
                byte ageRange5To = 65;
                byte ageRange6From = 66;
                byte ageRange6To = 100;
                List<SicknessesByAgeDTO> sortAgeList = new List<SicknessesByAgeDTO>();
                int sickEmployeesNumber = sickEmployeesList.Select(x => x.Employee.Id).Distinct().Count();
                foreach (var sickEmployee in sickEmployeesList)
                {
                    DateTime actuallyBirthday = new DateTime(
                        model.DateStart.Year,
                        sickEmployee.Employee.DateOfBirth.Month,
                        sickEmployee.Employee.DateOfBirth.Day);

                    int sickDays = 0;
                    int ageEmployee = new DateTime(sickEmployee.DateFrom.Subtract(sickEmployee.Employee.DateOfBirth).Ticks).Year - 1;

                    if (ageEmployee != ageRange1To && ageEmployee != ageRange2To
                        && ageEmployee != ageRange3To && ageEmployee != ageRange4To
                        && ageEmployee != ageRange5To)
                    {
                        //ageEmployee = new DateTime(sickEmployee.DateFrom.Subtract(sickEmployee.Employee.DateOfBirth).Ticks).Year - 1;

                        TimeSpan timeSpan = sickEmployee.DateTo - sickEmployee.DateFrom;
                        sickDays = timeSpan.Days + 1;

                        sortAgeList.Add(new SicknessesByAgeDTO()
                        {
                            AgeEmployee = ageEmployee,
                            SickDaysNumber = sickDays,
                            SickEmployeesNumber = sickEmployeesNumber,
                            SickId = sickEmployee.Id //TODO: ta linia wprowadzona jest tylko do sprawdzenia poprawności działania - usunąć
                        });
                    }
                    else
                    {
                        int sickDaysBeforeBirthday = actuallyBirthday.Day - sickEmployee.DateFrom.Day;
                        sortAgeList.Add(new SicknessesByAgeDTO()
                        {
                            AgeEmployee = ageEmployee,
                            SickDaysNumber = sickDaysBeforeBirthday,
                            SickEmployeesNumber = sickEmployeesNumber,
                            SickId = sickEmployee.Id //TODO: ta linia wprowadzona jest tylko do sprawdzenia poprawności działania - usunąć
                        });
                        ageEmployee = new DateTime(sickEmployee.DateTo.Subtract(sickEmployee.Employee.DateOfBirth).Ticks).Year - 1;
                        int sickDaysAfterBirthday = sickEmployee.DateTo.Day - actuallyBirthday.Day + 1;
                        sortAgeList.Add(new SicknessesByAgeDTO()
                        {
                            AgeEmployee = ageEmployee,
                            SickDaysNumber = sickDaysAfterBirthday,
                            SickEmployeesNumber = sickEmployeesNumber,
                            SickId = sickEmployee.Id //TODO: ta linia wprowadzona jest tylko do sprawdzenia poprawności działania - usunąć
                        });
                    }
                }
                int range1TotalDays = sortAgeList
                    .Where(x => x.AgeEmployee >= ageRange1From && x.AgeEmployee <= ageRange1To)
                    .Sum(x => x.SickDaysNumber);

                int range2TotalDays = sortAgeList
                    .Where(x => x.AgeEmployee >= ageRange2From && x.AgeEmployee <= ageRange2To)
                    .Sum(x => x.SickDaysNumber);

                int range3TotalDays = sortAgeList
                    .Where(x => x.AgeEmployee >= ageRange3From && x.AgeEmployee <= ageRange3To)
                    .Sum(x => x.SickDaysNumber);

                int range4TotalDays = sortAgeList
                    .Where(x => x.AgeEmployee >= ageRange4From && x.AgeEmployee <= ageRange4To)
                    .Sum(x => x.SickDaysNumber);

                int range5TotalDays = sortAgeList
                    .Where(x => x.AgeEmployee >= ageRange5From && x.AgeEmployee <= ageRange5To)
                    .Sum(x => x.SickDaysNumber);

                int range6TotalDays = sortAgeList
                    .Where(x => x.AgeEmployee >= ageRange6From && x.AgeEmployee <= ageRange6To)
                    .Sum(x => x.SickDaysNumber);

                List<SicknessesByAgeDTO> sicknessesByAgeDTO = new List<SicknessesByAgeDTO>();

                sicknessesByAgeDTO.Add(new SicknessesByAgeDTO
                {
                    AgeRangeFrom = ageRange1From,
                    AgeRangeTo = ageRange1To,
                    TotalSickDays = range1TotalDays
                }); sicknessesByAgeDTO.Add(new SicknessesByAgeDTO
                {
                    AgeRangeFrom = ageRange2From,
                    AgeRangeTo = ageRange2To,
                    TotalSickDays = range2TotalDays
                }); sicknessesByAgeDTO.Add(new SicknessesByAgeDTO
                {
                    AgeRangeFrom = ageRange3From,
                    AgeRangeTo = ageRange3To,
                    TotalSickDays = range3TotalDays
                }); sicknessesByAgeDTO.Add(new SicknessesByAgeDTO
                {
                    AgeRangeFrom = ageRange4From,
                    AgeRangeTo = ageRange4To,
                    TotalSickDays = range4TotalDays
                }); sicknessesByAgeDTO.Add(new SicknessesByAgeDTO
                {
                    AgeRangeFrom = ageRange5From,
                    AgeRangeTo = ageRange5To,
                    TotalSickDays = range5TotalDays
                }); sicknessesByAgeDTO.Add(new SicknessesByAgeDTO
                {
                    AgeRangeFrom = ageRange6From,
                    AgeRangeTo = ageRange6To,
                    TotalSickDays = range6TotalDays
                });
                return Ok(sicknessesByAgeDTO);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> CategorizationSicknessesBySexType([FromUri] RangeDatesDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Błąd w dacie.");
                }
                if (model.DateStart >= model.DateEnd)
                {
                    return BadRequest("Data od jest późniejszą niż data do.");
                }
                var sickEmployeesList = await CreateSickEmployeesInDatesRangeList(model.DateStart, model.DateEnd);
                if (sickEmployeesList.Count == 0)
                {
                    return Ok("Brak danych spełniających kryteria.");
                }
                List<SicknessesBySexTypeDTO> sicknessesBySexTypeList = new List<SicknessesBySexTypeDTO>();
                sicknessesBySexTypeList.Add(new SicknessesBySexTypeDTO()
                {
                    SexType = SexType.Female,
                    SickEmployeesNumber = sickEmployeesList.Where(x => x.Employee.SexType == SexType.Female).Count()
                });
                sicknessesBySexTypeList.Add(new SicknessesBySexTypeDTO()
                {
                    SexType = SexType.Male,
                    SickEmployeesNumber = sickEmployeesList.Where(x => x.Employee.SexType == SexType.Male).Count()
                });
                return Ok(sicknessesBySexTypeList);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> CategorizationSicknessesByDepartments([FromUri] RangeDatesDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Błąd w dacie.");
                }
                if (model.DateStart >= model.DateEnd)
                {
                    return BadRequest("Data od jest późniejszą niż data do.");
                }
                var sickEmployeesList = await CreateSickEmployeesInDatesRangeList(model.DateStart, model.DateEnd);
                if (sickEmployeesList.Count == 0)
                {
                    return Ok("Brak danych spełniających kryteria.");
                }
                List<SicknessesByDepartmentsDTO> sicknessesByDepartmentsList = new List<SicknessesByDepartmentsDTO>();
                var departmentsList = sickEmployeesList
                    .Select(x => x.Employee.Department)
                    .Distinct()
                    .ToList();
                foreach (var item in departmentsList)
                {
                    sicknessesByDepartmentsList.Add(new SicknessesByDepartmentsDTO()
                    {
                        Department = item,
                        SickEmployeesNumber = sickEmployeesList.Where(x => x.Employee.Department == item).Count()
                    });
                }
                return Ok(sicknessesByDepartmentsList);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> CategorizationByWorkerType([FromUri] RangeDatesDTO model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Błąd w dacie.");
                }
                if (model.DateStart >= model.DateEnd)
                {
                    return BadRequest("Data od jest późniejszą niż data do.");
                }
                var sickEmployeesList = await CreateSickEmployeesInDatesRangeList(model.DateStart, model.DateEnd);
                if (sickEmployeesList.Count == 0)
                {
                    return Ok("Brak danych spełniających kryteria.");
                }
                int mentalQuantity = sickEmployeesList.Where(x => x.Employee.WorkerType == WorkerType.Mental).Count();
                int physicalQuantity = sickEmployeesList.Where(x => x.Employee.WorkerType == WorkerType.Physical).Count();

                List<SicknessesByWorkerTypeDTO> sicknessesByWorkerTypeList = new List<SicknessesByWorkerTypeDTO>();
                sicknessesByWorkerTypeList.Add(new SicknessesByWorkerTypeDTO()
                {
                    WorkerType = WorkerType.Mental,
                    SickEmployeesNumber = mentalQuantity
                }); 
                sicknessesByWorkerTypeList.Add(new SicknessesByWorkerTypeDTO()
                {
                    WorkerType = WorkerType.Physical,
                    SickEmployeesNumber = physicalQuantity
                });
                return Ok(sicknessesByWorkerTypeList);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        private async Task<List<Sickness>> CreateSickEmployeesInDatesRangeList(DateTime rangeDateFrom, DateTime rangeDateTo)
        {
            List<Sickness> sickEmployeesList = null;
            using (var context = new SicknessContext())
            {
                sickEmployeesList = await context.Sickness.Include(x => x.Employee)
                    .Where
                    (sick => sick.DateFrom >= rangeDateFrom && sick.DateFrom <= rangeDateTo ||
                    sick.DateTo >= rangeDateFrom && sick.DateTo <= rangeDateTo ||
                    sick.DateFrom < rangeDateFrom && sick.DateTo > rangeDateTo).
                    ToListAsync();
            }
            return sickEmployeesList;
        }
        [HttpGet]
        public async Task<IHttpActionResult> GetSicknessesFirstNameLastName()
        {
            Employee employee = new Employee();
            try
            {
                List<Sickness> sickEmployeesList = null;

                using (var context = new SicknessContext())
                {
                    sickEmployeesList = await context.Sickness.Include(x => x.Employee).ToListAsync();
                }
                if (sickEmployeesList.Any())
                {
                    return Ok(sickEmployeesList);
                }
                else
                {
                    return BadRequest("Pobieranie danych nie powiodło się.");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteEmployee([FromUri] int receivedId)
        {
            try
            {
                bool correctId = int.TryParse(receivedId.ToString(), out int idEmployee);

                if (correctId && idEmployee > 0)
                {
                    using (var context = new SicknessContext())
                    {
                        var employeeToDelete = await context.Employee.Where(x => x.Id == idEmployee).SingleAsync();
                        context.Employee.Remove(employeeToDelete);
                        await context.SaveChangesAsync();
                    }
                    return Ok();
                }
                else
                {
                    return BadRequest("Błąd danych pracownika");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        [Obsolete]
        [HttpPatch]
        public async Task<IHttpActionResult> SzybkiPatch([FromUri] int id, DateTime urodziny)
        {
            Tuple<int, string> tuple = new Tuple<int, string>(2, "dwa");
            (int Age, string Name) tuple2 = (2, "darek");
            List<(int Age, string Name)> tuple3 = new List<(int Age, string Name)>()
            {
                 (2, "Darek"),
                 (3, "Emilia"),
                 (4, "Patryk")
            };

            using (var context = new SicknessContext())
            {
                var updateEmoployee = context.Employee.Where(x => x.Id == id).SingleOrDefault();
                updateEmoployee.DateOfBirth = urodziny;
                await context.SaveChangesAsync();
                return Ok();
            }
        }
    }
}